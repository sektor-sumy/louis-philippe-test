<?php
namespace App\Rest;


use App\Entity\BlogPost;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PostNormalizer
 */
class PostNormalizer
{
    /**
     * @param BlogPost $blogPost
     * @param bool     $isAdmin
     *
     * @return array
     */
    public function normalizeBlogPost(BlogPost $blogPost, $isAdmin)
    {
        $author = null;
        if (!empty($blogPost->getAuthor())) {
            $author = [
                'firstname' => $blogPost->getAuthor()->getFirstName(),
                'lastname' => $blogPost->getAuthor()->getLastName(),
            ];
        }
        $postNormalizer = [
            'slug' => $blogPost->getSlug(),
            'title' => $blogPost->getTitle(),
            'content' => $blogPost->getContent(),
            'created_at' => $blogPost->getCreatedAt()->format('Y-m-d'),
            'author' => $author,
        ];
        if ($isAdmin) {
            $postNormalizer['status'] = $blogPost->getStatus();
        }

        return $postNormalizer;
    }

    /**
     * @param BlogPost[]|ArrayCollection $blogPosts
     * @param bool                       $isAdmin
     * @return array
     */
    public function normalizeBlogPosts($blogPosts, $isAdmin)
    {
        $blogPostsNormalize = [];
        foreach ($blogPosts as $blogPost) {
            $blogPostsNormalize[] = $this->normalizeBlogPost($blogPost, $isAdmin);
        }

        return $blogPostsNormalize;
    }
}
