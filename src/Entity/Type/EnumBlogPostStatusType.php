<?php

namespace App\Entity\Type;

/**
 * Class EnumBlogPostStatusType
 */
class EnumBlogPostStatusType extends EnumType
{
    const DRAFT = 'draft';
    const PUBLISH  = 'publish';

    protected static $name = 'enum_blog_post_status_type';

    protected static $values = [
        self::DRAFT,
        self::PUBLISH,
    ];
}
