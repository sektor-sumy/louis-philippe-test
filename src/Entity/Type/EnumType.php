<?php

namespace App\Entity\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class EnumType
 */
abstract class EnumType extends Type
{
    /**
     * @var string
     */
    protected static $name;

    /**
     * @var array
     */
    protected static $values = [];

    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $values = array_map(function($val) { return "'".$val."'"; }, $this->getValues());

        return "ENUM(".implode(", ", $values).") COMMENT '(DC2Type:".$this->getName().")'";
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return static::$name;
    }

    /**
     * @return array
     */
    public static function getValues()
    {
        return static::$values;
    }

    /**
     * @return array
     */
    public static function getList()
    {
        return static::createList(static::getValues());
    }

    /**
     * @return array
     */
    public static function getValuesForForm()
    {
        $values = static::getValues();

        return array_combine($values, $values);
    }

    /**
     * @param  array $array
     * @return array
     */
    protected static function createList(array $array)
    {
        $list = [];
        foreach ($array as $value) {
            $list[$value] = static::$name . '_' . $value;
        }

        return $list;
    }
}
