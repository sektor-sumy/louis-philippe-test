<?php

namespace App\Form\Type;

use App\Entity\BlogPost;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DebateFormType
 */
class BlogPostFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('content', TextType::class)
            ->add('status', TextType::class);
        if ($options['is_create']) {
            $builder->add('slug', TextType::class);
        };
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
            'csrf_protection' => false,
            'validation_groups' => 'BlogPost',
            'is_create' => false,

        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'blog_post';
    }
}
