<?php

namespace App\DataFixtures\ORM;

use App\Entity\BlogPost;
use App\Entity\Type\EnumBlogPostStatusType;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadBlogPostsData
 */
class LoadBlogPostsData extends Fixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $em = $this->container->get('doctrine')->getManager();
        $admin = $em->getRepository(User::class)->findOneBy([
            'email' => 'admin@gmail.com',
        ]);
        $this->create($admin, 'test-admin-slug', 'test title 1', $em);
        $this->create($admin, 'test-admin-slug-2', 'test title 2', $em);
        $this->create($admin, 'test-admin-slug-3', 'test title 3', $em);

        $user1 = $em->getRepository(User::class)->findOneBy([
            'email' => 'testUser@gmail.com',
        ]);
        $this->create($user1, 'test-user1-slug', 'test user1 title 1', $em);
        $this->create($user1, 'test-user1-slug-2', 'test user1 title 2', $em);
        $this->create($user1, 'test-user1-slug-3', 'test user1 title 3', $em);

        $user2 = $em->getRepository(User::class)->findOneBy([
            'email' => 'testUser2@gmail.com',
        ]);
        $this->create($user2, 'test-user2-slug', 'title 1', $em);
        $this->create($user2, 'test-user2-slug-2', 'title 2', $em);
        $this->create($user2, 'test-user2-slug-3', 'title 3', $em);

        $this->create(null, 'test-null-slug', 'title null 1', $em);
        $this->create(null, 'test-null-slug-2', 'title null 2', $em);
        $this->create(null, 'test-null-slug-3', 'title null 3', $em);
    }

    /**
     * @param User|null $author
     * @param string    $slug
     * @param string    $title
     * @param mixed     $em
     */
    public function create($author, $slug, $title, $em)
    {
        $post = new BlogPost();

        $post->setAuthor($author)
            ->setContent('Test content')
            ->setStatus(EnumBlogPostStatusType::PUBLISH)
            ->setTitle($title)
            ->setSlug($slug);

        $em->persist($post);
        $em->flush();
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return 3;
    }
}
