<?php

namespace App\DataFixtures\ORM;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 */
class LoadUserData extends Fixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->createUser('admin@gmail.com', User::ROLE_ADMIN);
        $this->createUser('testUser@gmail.com', User::ROLE_USER);
        $this->createUser('testUser2@gmail.com', User::ROLE_USER);
    }

    /**
     * @param string $email
     * @param string $role
     */
    public function createUser($email, $role)
    {
        /** @var  UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');

        $dateOfBirth = new \DateTime();
        /** @var User $user */
        $user = $userManager->createUser();
        $user->setLastName('test');
        $user->setFirstName('test');
        $user->setEmail($email);
        $user->setPlainPassword('111111');
        $user->setEnabled(true);
        $user->setRoles([$role]);
        $user->setDateOfBirth($dateOfBirth);

        $userManager->updateUser($user, true);
    }


    /**
     * @return int
     */
    public function getOrder()
    {
        return 2;
    }
}
