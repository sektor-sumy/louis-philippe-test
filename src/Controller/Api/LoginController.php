<?php

namespace App\Controller\Api;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LoginController
 */
class LoginController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @Route("/login", name="login")
     *
     * @param Request $request
     */
    public function login(Request $request)
    {
    }
}
