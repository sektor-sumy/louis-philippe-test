<?php

namespace App\Controller\Api;

use App\Entity\Type\EnumBlogPostStatusType;
use App\Entity\User;
use App\Form\Type\BlogPostFormType;
use App\Rest\PostNormalizer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Entity\BlogPost;
use Swagger\Annotations as SWG;

/**
 * Brand controller.
 *
 * @Route("/blog-post")
 */
class BlogPostController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/blog-post/posts",
     *     summary="Get posts",
     *     description="Get posts",
     *     operationId="getPosts",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *     )
     * )
     *
     * @FOSRest\Get("/posts")
     *
     * @return array
     */
    public function getPostsAction()
    {
        $repository = $this->getDoctrine()->getRepository(BlogPost::class);

        $posts = $repository->findBy(['status' => EnumBlogPostStatusType::PUBLISH]);

        return $this->getPostNormalizer()->normalizeBlogPosts($posts, $this->isGranted(User::ROLE_ADMIN));
    }

    /**
     * @SWG\Get(
     *     path="/api/blog-post/post/{slug}",
     *     summary="Get post",
     *     description="Get post",
     *     operationId="getPost",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *     )
     * )
     *
     * @FOSRest\Get("/post/{slug}")
     *
     * @return array
     */
    public function getPostAction($slug)
    {
        $repository = $this->getDoctrine()->getRepository(BlogPost::class);

        // query for a single Product by its primary key (usually "id")
        $post = $repository->findOneBy([
            'author' => $this->getUser(),
            'slug' => $slug,
        ]);

        return $this->getPostNormalizer()->normalizeBlogPost($post, $this->isGranted(User::ROLE_ADMIN));
    }

    /**
     * @SWG\Post(
     *     path="/api/blog-post/post",
     *     summary="save post",
     *     description="save post",
     *     operationId="postPost",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="title",
     *         in="query",
     *         description="Title",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="content",
     *         in="query",
     *         description="Content",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="slug",
     *         in="query",
     *         description="Slug unique",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *     )
     * )
     *
     * @FOSRest\Post("/post")
     *
     * @param Request $request
     *
     * @return array|static
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(BlogPostFormType::class, new BlogPost(), [
            'is_create' => true,
        ]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            /** @var BlogPost $blogPost */
            $blogPost = $form->getData();
            $blogPost->setAuthor($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($blogPost);
            $em->flush();

            return $this->getPostNormalizer()->normalizeBlogPost($blogPost, $this->isGranted(User::ROLE_ADMIN));
        }

        return View::create($form, Response::HTTP_BAD_REQUEST, []);
    }

    /**
     * @SWG\Put(
     *     path="/api/blog-post/post/{slug}",
     *     summary="update post",
     *     description="update post",
     *     operationId="putPost",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="title",
     *         in="query",
     *         description="Title",
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="content",
     *         in="query",
     *         description="Content",
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *     )
     * )
     *
     * @FOSRest\Put("/post/{slug}")
     *
     * @param string  $slug
     * @param Request $request
     *
     * @return array|static
     */
    public function putPost($slug, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $blogPost = $em->getRepository(BlogPost::class)->findOneBy(
            [
                'slug' => $slug,
                'author' => $this->getUser(),
            ]
        );
        if ($blogPost) {
            $form = $this->createForm(BlogPostFormType::class, $blogPost, [
                'is_create' => false,
            ]);
            $form->submit($request->request->all(), false);

            if ($form->isValid()) {
                /** @var BlogPost $blogPost */
                $blogPost = $form->getData();
                $em->flush();

                return $this->getPostNormalizer()->normalizeBlogPost($blogPost, $this->isGranted(User::ROLE_ADMIN));
            }

            return View::create($form, Response::HTTP_BAD_REQUEST);
        } else {
            return View::create($blogPost, Response::HTTP_BAD_REQUEST, []);
        }
    }

    /**
     * @SWG\Delete(
     *     path="/api/blog-post/post/{slug}",
     *     summary="delete post",
     *     description="delete post",
     *     operationId="deletePost",
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *     )
     * )
     *
     * @FOSRest\Delete("/post/{slug}")
     *
     * @param string  $slug
     * @return array|static
     */
    public function deletePost($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $blogPost = $em->getRepository(BlogPost::class)->findOneBy(
            [
                'slug' => $slug,
                'author' => $this->getUser(),
            ]
        );
        if ($blogPost) {
            $em->remove($blogPost);
            $em->flush();
        } else {
            return View::create($blogPost, Response::HTTP_BAD_REQUEST, []);
        }
        // In case our PUT was a success we need to return a 200 HTTP OK response with the object as a result of PUT
        return View::create($blogPost, Response::HTTP_OK);
    }


    /**
     * @return PostNormalizer
     */
    protected function getPostNormalizer()
    {
        return $this->get(PostNormalizer::class);
    }
}
